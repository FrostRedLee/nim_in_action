import json

type
  Message* = object
    username*: string
    message*: string

proc parseMessage*(data: string): Message =
  let dataJson = parseJson(data) # jsonNode类型，它有个kind字段表示7个具体类型中的某个
  result.username = dataJson["username"].getStr()
  result.message = dataJson["message"].getStr()
proc createMessage*(username, message: string): string =
  # % 运算符将string转为JsonNode
  result = $(%{
    # {:}构造table
    "username": %username,
    "message": %message
    }) & "\c\l" # "\c\l"为分隔符

when isMainModule:
  block:
    let data = """{"username": "John", "message": "Hi!"}"""
    let parsed = parseMessage(data)
    doAssert parsed.username == "John"
    doAssert parsed.message == "Hi!"
  block:
    # 中间没有空格
    let expected = """{"username":"dom","message":"hello"}""" & "\c\l"
    doAssert createMessage("dom", "hello") == expected