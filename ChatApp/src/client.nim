import os
import threadpool
import asyncdispatch, asyncnet

import protocol

proc connect(socket: AsyncSocket, serverAddr: string) {.async.} =
  echo("Connecting to ", serverAddr)
  await socket.connect(serverAddr, 7688.Port)
  echo("Connected!")

  while true:
    let line = await socket.recvLine()
    let parsed = parseMessage(line)
    echo(parsed.username, " said ", parsed.message)

echo("Chat application started")

if paramCount() == 0:
  quit("Please specify the servers address, e.g. ./clinet localhost")

let serverAddr = paramStr(1)
var socket = newAsyncSocket()
asyncCheck socket.connect(serverAddr)
echo("Connecting to ", serverAddr)

var messageFlowVar:FlowVar[string] = spawn stdin.readLine()
while true:
  if messageFlowVar.isReady():
    # spawn return FlowVar[T], and meybe empty, use ^message,
    # ^ will block current thread until message not empty
    let message = createMessage("Anonymous", ^messageFlowVar)
    asyncCheck socket.send(message)
    messageFlowVar = spawn stdin.readLine()

  poll()