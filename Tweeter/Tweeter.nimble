# Package

version       = "0.1.0"
author        = "Frost Red Lee"
description   = "Nim in action ch7"
license       = "MIT"

bin = @["tweeter"]

# Dependencies

requires @["nim >= 0.16.0", "jester >= 0.1.0"]

