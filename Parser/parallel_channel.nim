import threadpool

var resultChan: Channel[int]
open(resultChan)

proc incr(x: int) =
  var counter = 0
  for i in 0..<x:
    counter.inc
  resultChan.send(counter)

spawn incr(10_000)
spawn incr(10_000)
sync()

var total = 0
for i in 0..<resultChan.peek():
  total = total + resultChan.recv() 

echo(total)