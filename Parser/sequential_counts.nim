import parseutils, os

proc parse(line: string, domainCode, pageTitle: var string, countViews, totalSize: var int) =
  var i = 0

  domainCode.setLen(0)
  # 必须这样写,不能inc(i)
  i.inc parseUntil(line, domainCode, {' '}, i)
  inc(i)

  pageTitle.setLen(0)
  i.inc parseUntil(line, pageTitle, {' '}, i)
  inc(i)

  countViews = 0
  i.inc parseInt(line, countViews, i)
  inc(i)

  totalSize = 0
  i.inc parseInt(line, totalSize, i)
  inc(i)

proc readPageCounts(filename: string) =
  var domainCode = ""
  var pageTitle = ""
  var countViews = 0
  var totalSize = 0

  var mostPopular = ("", "", 0, 0)
  for line in filename.lines:
    parse(line, domainCode, pageTitle, countViews, totalSize)
    if domainCode == "en" and countViews > mostPopular[2]:
      mostPopular = (domainCode, pageTitle, countViews, totalSize)
  echo("Most popular: ", mostPopular)

when isMainModule:
  const file = "pagecounts-20160802-050000"
  let filename = getCurrentDir() / file
  readPageCounts(filename)