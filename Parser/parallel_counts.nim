import parseutils, os
import threadpool, strutils

type
  Stats = ref object
    domainCode, pageTitle: string
    countViews, totalSize: int
proc newStats(): Stats =
  # 使之不为默认的nil
  Stats(domainCode: "", pageTitle: "", countViews: 0, totalSize: 0)
proc `$`(this: Stats): string =
  "(domainCode: $#, pageTitle: $#, countViews: $#, totalSize: $#)" % [this.domainCode, this.pageTitle, $this.countViews, $this.totalSize]

proc parse(line: string, domainCode, pageTitle: var string, countViews, totalSize: var int) =
  if line.len() == 0:
    return

  var i = 0
  domainCode.setLen(0)
  # 必须这样写,不能inc(i)
  i.inc parseUntil(line, domainCode, {' '}, i)
  inc(i)

  pageTitle.setLen(0)
  i.inc parseUntil(line, pageTitle, {' '}, i)
  inc(i)

  countViews = 0
  i.inc parseInt(line, countViews, i)
  inc(i)

  totalSize = 0
  i.inc parseInt(line, totalSize, i)
  inc(i)

proc parseChunk(chunk: string): Stats =
  result = newStats()
  var domainCode = ""
  var pageTitle = ""
  var countViews = 0
  var totalSize = 0

  for line in chunk.splitLines():
    parse(line, domainCode, pageTitle, countViews, totalSize)
    if domainCode == "en" and countViews > result.countViews:
      result = Stats(domainCode: domainCode, pageTitle: pageTitle, countViews: countViews, totalSize: totalSize)

proc readPageCounts(filename:string, chunkSize = 1_000_0) =
  var file = open(filename)
  var responses = newSeq[FlowVar[Stats]]()
  var buffer = newString(chunkSize)
  var oldBufferLen = 0 # 上一次剩下的字符长度

  while not endOfFile(file):
    let reqSize = chunkSize - oldBufferLen
    # 从file中读reqSize个字符到buffer[oldBufferLen]
    let readSize = file.readChars(buffer, oldBufferLen, reqSize) + oldBufferLen
    var chunkLen = readSize # 这个块的长度

    while chunkLen >= 0 and buffer[chunkLen - 1] notin NewLines:
      chunkLen.dec
    responses.add(spawn parseChunk(buffer[0..<chunkLen]))

    oldBufferLen = readSize - chunkLen # 还剩了多少个
    buffer[0..<oldBufferLen] = buffer[readSize-oldBufferLen .. ^1]

  var mostPopular = newStats()
  for resp in responses:
    let statistic = ^resp
    if statistic.countViews > mostPopular.countViews:
      mostPopular = statistic
    
  echo("Most poplular: ", mostPopular)
  file.close()

when isMainModule:
  const file = "pagecounts-20160802-050000"
  let filename = getCurrentDir() / file
  readPageCounts(filename)