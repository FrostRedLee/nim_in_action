import threadpool
import os
import locks

var counterLock: Lock
var counter {.guard:counterLock.} = 0

proc incre(x: int) =
  for i in 0..<x:
    withLock counterLock:
      var v = counter
      v.inc
      counter = v

spawn incre(10_000)
spawn incre(10_000)
sync()
echo(counter)